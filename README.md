# 2023-ta-rg-rz Waveguides Monotonicity

## License

Copyright (c) 2023, Tilo Arens, Roland Griesmaier

This software is released under GNU General Public License, Version 3.
The full license text is provided in the file LICENSE included in this repository 
or can be obtained from http://www.gnu.org/licenses/


## Content of this project

This project contains the software developed for the paper:

*Monotonicity-based shape reconstruction for an inverse scattering problem in a waveguide* 

by T. Arens, R. Griesmaier, and R. Zhang.

The following versions of the paper are available:
- [ ] [CRC Preprint 2022/69, December 2022](https://www.waves.kit.edu/downloads/CRC1173_Preprint_2022-69.pdf)

The code is in the Matlab programming language. 


## Requirements

The following additional software is necessary to run the code in this project:
- [ ] a recent version of Matlab
- [ ] the BIEPack Matlab library for solving boundary integral equations
  available from https://gitlab.kit.edu/kit/ianm/ag-ip/software/biepack and all
  dependent software to activate the evaluation of periodic Green's
  functions
- [ ] the export_fig Matlab package, available from https://github.com/altmany/export_fig

Set the Matlab path so that the BIEPack files and export_fig will be found.


## Running the code

All programs are contained in the src/ directory. The plots for the paper are created in the directory figures/.

Below is a brief description of each program file. See the individual program files for a detailed explanations.

**src/plot_of_figure_?.m**

These are the driver files to create the plots in section 6 of the paper (Figures 6.1, 6.2 and 6.3). These
programs are called without any parameters and will produce the plots in these figures in PNG format.

**src/operator_N.m**

This program computes a discretization of the near field operator for a scattering problem using the BIEPack
library. A number of scattering problems are solved and the scattered field is evaluated in certain receiver
points. These values form the column of the near field matrix that is returned by this function. 

**src/indicator_func.m**

For a grid of points, this function computed the value of the indicator function proposed in the paper. 

**src/operator_T_B.m**

The indicator function uses the difference of the real part of the near field operator and the test operator 
T_B. Given some point and the dimensions of the test domain, a discretization of this test operator is
computed y this function.
