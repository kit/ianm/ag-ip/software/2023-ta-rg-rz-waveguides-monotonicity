function [] = plot_of_figure_2()

H = 5;
R = 6;

curve = BIEPack.geom.Circle([1.5; 3.5], 0.35);

k_out = 11.0;
k_in = sqrt(2)*k_out;
M = 17;

N_q = operator_N(H, curve, k_out, k_in, R, M, 512, 80, true);
%lambda = eig(N_q);

% Define the plots
a = 0.01;
alpha = [0.01 0.025 0.0375 0.05];
fig_nr = 1:length(alpha);
indicat = cell(1, length(fig_nr));
x_max = 5;
%step = 0.1;
step = 0.05;
s = linspace(-pi, pi, 201);
obs_height = 25;

% figure(length(alpha)+1);
% semilogy(abs(lambda));

for j = 1:length(fig_nr)
    [indicat{j}, Xi1, Xi2] = indicator_func(N_q, alpha(j), k_out, H, R, x_max, step, a, true);

    figure(fig_nr(j))
    colormap(parula);
    surf(Xi1, Xi2, indicat{j});
    hold on
    [x, y] = curve.eval(s, 0);
    plot3(x, y, obs_height*ones(size(x)), 'w');
    hold off
    view(2);
    axis equal
    axis([-5 5 0 H]);
    shading interp
    colorbar
    set(gcf, 'Color', 'w');
    export_fig(sprintf('../figures/circle_off_center_k_%4.2f_alpha_%4.2f.png', k_out, alpha(j)));
end

end

