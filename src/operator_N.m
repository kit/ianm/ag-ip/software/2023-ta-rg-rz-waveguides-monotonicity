function N_mat_ct = operator_N(H, curve, k_out, k_in, R, M, N, N_quad_ct, debug_mode)
% 
% The function generates a discretization for the near field operator N for
% a penetrable obstacle in a waveguide with Neumann boundary conditions.
%
% The waveguide is \R \times (0, 'H'). The obstacle is given by the boundary
% curve 'curve' and should be a BIEPack.geom.ParamCurve instance. It must
% be contained in the waveguide.
%
% The numbers 'k_out' and 'k_in' are the wavenumbers outside and inside the
% obstacle, respectively. The positive number 'R' specifes the
% x_1-coordinate of the vertical lines containing the source and receiver 
% points. 
%
% The space L^2(C_R) is discretized using the basis induced by the Neumann
% eigenfunctions of the Laplacian on (0, H). If one of these are used as
% the density in the conjugated single layer potential, the series for the
% Green's function reduces to just one term. We use the first M+1
% eigenfunctions, hence the returned matrix will be of size 2M+2 x 2M+2.
%
% To compute the coefficients of the scattered field with respect to the
% basis, the composite trapezoidal rule with M+1 points is used on each
% vertical line. As the field can be extended as an even function for
% negative x_2 to a 2H-periodic function, the rule is highly accurate.
%
% The parameter 'N' specifies the number of unknowns to use in the boundary
% integral equation method.
%
% The parameter debug_mode is optional and defaults to false. If it is
% true, some tests whether the boundary data is correct, are carried out.

% set default value of parameters
if (nargin < 8)
    N_quad_ct = 80;
end

if (nargin < 9)
    debug_mode = false;
end

% shorten names of BIEPack classes
import BIEPack.*

% allocate memory for the output and define source/receiver positions
%N_mat_gl = zeros(2*M+2, 2*M+2);
N_mat_ct = zeros(2*M+2, 2*M+2);

% quadrature points on C_R and corresponding weights for evaluating scalar 
% products with basis functions
y_quad_ct = linspace(0, H, N_quad_ct+1).';
w_ct = H/N_quad_ct * ones(1, N_quad_ct+1);
w_ct([1, N_quad_ct+1]) = 0.5 * w_ct([1, N_quad_ct+1]);

%[y_quad_gl, w_gl] = BIEPack.utils.quadGaussLegendre(40, 0, H);
%y_quad_gl = y_quad_gl.';

%y1_gl = [-R * ones(size(y_quad_gl)); R * ones(size(y_quad_gl))];
%y2_gl = [y_quad_gl; y_quad_gl];
y1_ct = [-R * ones(size(y_quad_ct)); R * ones(size(y_quad_ct))];
y2_ct = [y_quad_ct; y_quad_ct];


% define the mesh
mesh = meshes.ParamCurveMesh(curve, N);
space = spaces.TrigPol(mesh);

% define all the integral operators
slop_ext = ops.HelmholtzNwgSLOp(space, space, k_out, H);
dlop_ext = ops.HelmholtzNwgDLOp(space, space, k_out, H);
adlop_ext = ops.HelmholtzNwgADLOp(space, space, k_out, H);
slop_int = ops.HelmholtzSLOp(space, space, k_in);
dlop_int = ops.HelmholtzDLOp(space, space, k_in);
adlop_int = ops.HelmholtzADLOp(space, space, k_in);
hsing_dif_op = ops.HelmholtzNwgHsingDifOp(space, space, k_out, k_in, H);
slpot = ops.HelmholtzNwgSLPot(space, space, k_out, H);
dlpot = ops.HelmholtzNwgDLPot(space, space, k_out, H);

% Define the block operator A in the IE system (I - A) phi = psi in
% the Kress-Roach integral equation formulation for a transmission problem.
A = {(-1)*dlop_ext + dlop_int, slop_ext + (-1)*slop_int;
    (-1)*hsing_dif_op, adlop_ext + (-1)*adlop_int};

% Set up the Nyström method for solving the IE system
nystroemMethod = methods.NystroemPeriodicLog();
nystroemMethod.setOperators(A);
    
if (debug_mode)
    % Operators for checking that the boundary data is sensible. It 
    % corresponds to Cauchy data of a solution of the Helmholtz equation in 
    % the obstacle, so it should be in the kernel of the Calderon projector.
    slop_ff_out = ops.HelmholtzSLOp(space, space, k_out);
    dlop_ff_out = ops.HelmholtzDLOp(space, space, k_out);
    calderon_dir = 2.0*dlop_ff_out;
    calderon_neu = (-2.0)*slop_ff_out;
    calderon_dir_mat = calderon_dir.getImplementation('NystroemPeriodicLog');
    calderon_neu_mat = calderon_neu.getImplementation('NystroemPeriodicLog');
    fprintf('Exterior wave number is %6.4f\n', k_out);
end

% for each source point, assign right hand side and solve
import BIEPack.utils.PeriodicGreensFunc
for j=1:2*M+2    
    mesh.evalDerivs(1);

    k_n = mod(j-1, M+1) * pi / H;
    beta_n_conj = sqrt(k_out^2 - k_n^2)';

    if j == 1
        ui_dirichlet = -0.5i / (sqrt(H)*beta_n_conj) * exp(-1i * beta_n_conj * (R + mesh.nodesX.'));
        ui_neumann = -0.5 / sqrt(H) * exp(-1i * beta_n_conj * (R + mesh.nodesX.')) .* mesh.normalAtNodeX.';
    elseif 2 <= j && j <= M+1
        ui_dirichlet = -0.5i / (sqrt(H/2)*beta_n_conj) * exp(-1i * beta_n_conj * (R + mesh.nodesX.')) .* cos(k_n * mesh.nodesY.'); 
        ui_neumann = -0.5 / sqrt(H/2) * exp(-1i * beta_n_conj * (R + mesh.nodesX.')) ...
            .* (cos(k_n * mesh.nodesY.') .* mesh.normalAtNodeX.' - 1i * k_n / beta_n_conj * sin(k_n * mesh.nodesY.') .* mesh.normalAtNodeY.');
    elseif j == M+2
        ui_dirichlet = -0.5i / (sqrt(H)*beta_n_conj) * exp(-1i * beta_n_conj * (R - mesh.nodesX.'));
        ui_neumann = 0.5 / sqrt(H) * exp(-1i * beta_n_conj * (R - mesh.nodesX.')) .* mesh.normalAtNodeX.'; 
    else
        ui_dirichlet = -0.5i / (sqrt(H/2)*beta_n_conj) * exp(-1i * beta_n_conj * (R - mesh.nodesX.')) .* cos(k_n * mesh.nodesY.');
        ui_neumann = 0.5 / sqrt(H/2) * exp(-1i * beta_n_conj * (R - mesh.nodesX.')) ...
            .* (cos(k_n * mesh.nodesY.') .* mesh.normalAtNodeX.' + 1i * k_n / beta_n_conj * sin(k_n * mesh.nodesY.') .* mesh.normalAtNodeY.'); 
    end

    if (debug_mode)
        % check that the boundary data is sensible.
        residual = ui_dirichlet + calderon_dir_mat * ui_dirichlet + calderon_neu_mat * ui_neumann;
        fprintf('mode = %2d, k_n = %6.4f, boundary data Calderon residual: %e\n', j, k_n, norm(residual));
    end

    nystroemMethod.setRhs({-ui_dirichlet; -ui_neumann});
    phi = nystroemMethod.apply;
    
    u_scat_ct = dlpot.evalPotential(y1_ct, y2_ct, phi(1:N)) - slpot.evalPotential(y1_ct, y2_ct, phi(N+1:2*N));

    theta_l = 1/sqrt(H) * ones(size(y_quad_ct));
    N_mat_ct(1, j) = w_ct * (theta_l .* u_scat_ct(1:length(y_quad_ct)));
    N_mat_ct(M+2, j) = w_ct * (theta_l .* u_scat_ct(length(y_quad_ct)+1:end));
    for l=2:M+1
        k_l = (l-1)*pi/H;
        theta_l = sqrt(2/H) * cos(k_l * y_quad_ct);
        N_mat_ct(l, j) = w_ct * (theta_l .* u_scat_ct(1:length(y_quad_ct)));
        N_mat_ct(M+1+l, j) = w_ct * (theta_l .* u_scat_ct(length(y_quad_ct)+1:end));
    end
end

% fprintf('Difference %e\n', norm(N_mat_gl - N_mat_ct));

end