function [] = plot_of_figure_1()

H = 5;
R = 6;
x_max = 5;

curve = BIEPack.geom.Ellipse([0; 0], 0.6, 0.2);
curve.rotate(pi/6);
curve.translate([-0.4; 2.6]);

k_out = 6;
k_in = sqrt(2) * k_out;
M = 10;

N_q = operator_N(H, curve, k_out, k_in, R, M, 512, 80, true);

% Define the plots
a = 0.01;
%alpha = [0.1 0.2 0.5 0.75];
alpha = [0.05 0.06 0.1 0.2];
fig_nr = 1:length(alpha);
indicat = cell(1, length(fig_nr));
%step = 0.1;
step = 0.05;
s = linspace(-pi, pi, 201);
obs_height = 25;

% lambda = eig(N_q);
% figure(length(alpha)+1);
% semilogy(abs(lambda));

for j = 1:length(fig_nr)
    [indicat{j}, Xi1, Xi2] = indicator_func(N_q, alpha(j), k_out, H, R, x_max, step, a, true);

    figure(fig_nr(j))
    colormap(parula);
    surf(Xi1, Xi2, indicat{j});
    hold on
    [x, y] = curve.eval(s, 0);
    plot3(x, y, obs_height*ones(size(x)), 'w');
    hold off
    view(2);
    axis equal
    axis([-5 5 0 H]);
    shading interp
    colorbar
    set(gcf, 'Color', 'w');
    export_fig(sprintf('../figures/ellipse_central_k_6.0_alpha_%04.2f.png', alpha(j)));
end

end

