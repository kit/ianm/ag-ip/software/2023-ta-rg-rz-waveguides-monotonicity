function T_B = operator_T_B(H, k_out, R, xi1, xi2, a, M)
%
% Set up the operator T_B where B is a square centered at (xi_1, xi_2) with
% lateral length 2a. The operator is discretized by using the basis in
% L^2(C_R) induced by the Neumann eigenfunctions of the Laplacian,
%
% \theta_n(t) = sqrt(2/H) cos( n pi / H * t )   for n \geq 1
% \theta_0(t) = sqrt(1/H)
%
% The corresponding basis is
%
% g^1_n = ( theta_n, 0 ) ,   g^2_n = ( 0, theta_n )
%
% where the first component function is defined on C_{-, R}, the second one
% on C_{+, R}. Fixing the number M of the highest eigen function used, the
% resulting matrix is of size 2M+2 x 2M+2 and contains the scalar products
%
% A^(pq)_{jn} = ( T_B g^q_n , g^p_j )_{L^2(0, H)}  p,q = 1,2,  j,n = 0,...,M.
%
% As the Green's function is also expanded in the theta_n, only one term in
% the double series in the expression of T_B remains. This still envolves
% an integral over two exponential functions. This is computed by a
% Gauss-Legendre quadrature rule.

T_B = zeros(2*M+2, 2*M+2);

k_n = (0:M) * pi / H;
beta_n = sqrt(k_out^2 - k_n.^2).';
max_real_index = sum(imag(beta_n) == 0);

% % The number of Gauss-Legendre points for quadratur with respect to y_1
% N_gauss = 15;
% [y1, w] = BIEPack.utils.quadGaussLegendre(N_gauss, xi1-a, xi1+a);
% W = diag(w);
% 
% pre_factors = 0.25 * k_out^2 ./ beta_n ./ beta_n' .* gamma_n_m_eval(M, k_n, xi2, a, H);
% exp_plus = exp(1i * beta_n * (R + y1));
% exp_minus = exp(1i * beta_n * (R - y1));
% 
% T_B(1:M+1, 1:M+1) = exp_plus * W * exp_plus' .* pre_factors;
% T_B(1:M+1, M+2:end) = exp_plus * W * exp_minus' .* pre_factors;
% T_B(M+2:end, 1:M+1) = exp_minus * W * exp_plus' .* pre_factors;
% T_B(M+2:end, M+2:end) = exp_minus * W * exp_minus' .* pre_factors;

one_vec = ones(size(beta_n));
gamma_n_m = gamma_n_m_eval(M, k_n, xi2, a, H);
pre_factors = 0.25 * k_out^2 ./ beta_n ./ beta_n' .* gamma_n_m ...
    .* (exp(1i * R * (beta_n * one_vec.' - one_vec * beta_n')));

% block nu = mu = 1
denom = 1i * (beta_n * one_vec.' - one_vec * beta_n');
exp_plus = exp(denom * (xi1 + a));
exp_minus = exp(denom * (xi1 - a));
T_B_block = (exp_plus - exp_minus) ./ denom .* pre_factors;
diag_indeces = (1:M+2:(M+2)*(max_real_index-1)+1).';
T_B_block(diag_indeces) = 0.5 * a * k_out^2 ./ beta_n(1:max_real_index).^2 .* gamma_n_m(diag_indeces);
T_B(1:M+1, 1:M+1) = T_B_block;

% block nu = 2, mu = 2
denom = -1i * (beta_n * one_vec.' - one_vec * beta_n');
exp_plus = exp(denom * (xi1 + a));
exp_minus = exp(denom * (xi1 - a));
T_B_block = (exp_plus - exp_minus) ./ denom .* pre_factors;
T_B_block(diag_indeces) = 0.5 * a * k_out^2 ./ beta_n(1:max_real_index).^2 .* gamma_n_m(diag_indeces);
T_B(M+2:end, M+2:end) = T_B_block;

% block nu = 1, mu = 2
denom = 1i * (beta_n * one_vec.' + one_vec * beta_n');
exp_plus = exp(denom * (xi1 + a));
exp_minus = exp(denom * (xi1 - a));
T_B_block = (exp_plus - exp_minus) ./ denom .* pre_factors;
diag_indeces = ( (M+2)*max_real_index + (1:M+2:(M+2)*(M + 1 - max_real_index)) ).';
T_B_block(diag_indeces) = 0.5 * a * k_out^2 ./ abs(beta_n(max_real_index+1:end)).^2 .* gamma_n_m(diag_indeces) ...
    .* exp(-2 * R * abs(beta_n(max_real_index+1:end)));
T_B(1:M+1, M+2:end) = T_B_block;

% block nu = 2, mu = 1
denom = -1i * (beta_n * one_vec.' + one_vec * beta_n');
exp_plus = exp(denom * (xi1 + a));
exp_minus = exp(denom * (xi1 - a));
T_B_block = (exp_plus - exp_minus) ./ denom .* pre_factors;
T_B_block(diag_indeces) = 0.5 * a * k_out^2 ./ abs(beta_n(max_real_index+1:end)).^2 .* gamma_n_m(diag_indeces) ...
    .* exp(-2 * R * abs(beta_n(max_real_index+1:end)));
T_B(M+2:end, 1:M+1) = T_B_block;

end


% The matrix (gamma_n^m)_{nm} contains the scalar products of two Neumann
% functions over the interval (\xi_2-a, \xi_2 + a) \subseteq (0, H). It is
% symmetric.
function g_n_m = gamma_n_m_eval(M, k_n, xi2, a, H)

g_n_m = zeros(M+1, M+1);
k_n_nat = k_n(2:end);

% entry for n = m = 0
g_n_m(1, 1) = 2*a / H;

% other entries for first row and first column
v = sqrt(2) ./ (H * k_n_nat) .* (sin(k_n_nat*(xi2+a)) -  sin(k_n_nat*(xi2-a)));
g_n_m(1, 2:end) = v;
g_n_m(2:end, 1) = v.';

% assign the diagonal of the matrix except for first entry
g_n_m(M+3:M+2:end) = 2*a / H + (sin(2*k_n_nat*(xi2+a)) -  sin(2*k_n_nat*(xi2-a))) ./ (2 * k_n_nat * H);

% assign all the sub-diagonals
for l=1:M
    k_diff = k_n(2+l:end) - k_n(2:end-l);
    k_sum = k_n(2+l:end) + k_n(2:end-l);
    v = sin(k_diff*(xi2+a))  / (k_diff * H) + sin(k_sum*(xi2+a)) / (k_sum * H) ...
          - sin(k_diff*(xi2-a))  / (k_diff * H) - sin(k_sum*(xi2-a)) / (k_sum * H);
    g_n_m((M+1)*(l+1)+2:M+2:(M+1)^2-l) = v;
    g_n_m((M+2)+l+1:M+2:(M+1-l)*(M+1)) = v;
end

end
