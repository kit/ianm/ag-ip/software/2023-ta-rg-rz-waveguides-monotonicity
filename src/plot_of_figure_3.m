function [] = plot_of_figure_3()

H = 5;
R = 6;

curve = BIEPack.geom.Ellipse([0; 0], 0.5, 0.2);
curve.rotate(pi/6);
curve.translate([-1; 2.5]);

k_out = 6.0;
M = 9;

%alpha = [-0.01 -0.008 -0.007 -0.006 ];
alpha = [-0.01 -0.02 -0.03 -0.04];
q = -0.5 * ones(size(alpha));
fig_nr = 1:length(q);
a = 0.01;

indicat = cell(1, length(fig_nr));
x_max = 5;
step = 0.05;
s = linspace(-pi, pi, 201);
obs_height = 25;

for j = 1:length(fig_nr)

    N_q = operator_N(H, curve, k_out, sqrt((1+ q(j)))*k_out, R, M, 512, true);
    [indicat{j}, Xi1, Xi2] = indicator_func(N_q, alpha(j), k_out, H, R, x_max, step, a, false);

    figure(fig_nr(j))
    colormap(parula);
    surf(Xi1, Xi2, indicat{j});
    hold on
    [x, y] = curve.eval(s, 0);
    plot3(x, y, obs_height*ones(size(x)), 'w');
    hold off
    view(2);
    axis equal
    axis([-5 5 0 5]);
    shading interp
    colorbar
    set(gcf, 'Color', 'w');
    export_fig(sprintf('../figures/ellipse_neg_q_alpha_%05.3f.png', -alpha(j)))
end

% lambda = eig(N_q);
% figure(length(alpha)+1);
% semilogy(abs(lambda));


end

