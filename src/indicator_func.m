function [indicat, Xi1, Xi2] = indicator_func(op_N, alpha, k_out, H, R, x_max, mesh_step, a, q_positive)
%
% Compute the indicator function for finding an obstacle for a transmission
% problem in a Neumann waveguide of height H. The wave number in the
% waveguide is k_out.
%
% The near field measurements, which are taken on vertikal lines at +-R, 
% are contained in the matrix operator_N.
%
% The function creates a grid of points on [-x_max, xmax] x [0, H] with
% mesh size mesh_step. For each grid point, the operator T_B is computed for 
% B equal to a square with lateral length 2a centered at the grid point.
% The number of negative eigenvalues of the matrix
%
% Re(N) - alpha T_B
%
% is computed. This the value of the indicator for this point.
%
% The parameter q_positive should be true for the case q > 0, it should be
% false for the case -1 < q <= 0.

% threshold for negative eigenvalues. For now we define this as a constant.
if q_positive
    delta = -2e-6;  % for figure 1
    %delta = -2e-6;  % for figure 2
else
    delta = 2e-6;   % for figure 3
end

% obtain the number of source/receiver points from the dimension of
% operator_N
M = size(op_N, 1) / 2 - 1;

% grid in x1 direction: x_max_true is the largest value such that the
% square centred at that x_1 coordinate is completely inside the rectangle
% [-x_max, x_max] x [0, H]
N_x1 = floor(2*(x_max-a) / mesh_step);
x_max_true = mesh_step * N_x1 / 2;
x1 = linspace(-x_max_true, x_max_true, N_x1+1);

% grid in x2 direction. The centers of the first and last pixels are
% x2_offset away from the waveguide boundary.
bound_gap = 0.05;
N_x2 = floor((H-2*a-2*bound_gap) / mesh_step);
x2_offset = (H - N_x2*mesh_step) / 2;
x2 = linspace(x2_offset, H-x2_offset, N_x2+1);

[Xi1, Xi2] = meshgrid(x1, x2);
indicat = zeros(size(Xi1));

real_N = (op_N + op_N') / 2;

for j=1:N_x1+1
    fprintf('.');
    for l=1:N_x2+1
        T_B = operator_T_B(H, k_out, R, Xi1(l, j), Xi2(l, j), a, M);
        the_op = real_N - alpha * T_B;
        eigenvals = eig(the_op);
        if q_positive
            indicat(l, j) = sum(eigenvals < delta);
        else
            indicat(l, j) = sum(eigenvals > delta);
        end
    end
end
fprintf('\n\n');


end

